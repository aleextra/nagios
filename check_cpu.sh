#!/bin/bash
# THANK YOU: https://unix.stackexchange.com/questions/313178/get-per-core-cpu-load-in-shell-script
# JOHN https://unix.stackexchange.com/users/83834/john
# I just adapt this script for nagios requirements
#Calculation delay. Without a delay, there is no way to determine current 
#values. The content or /proc/stat is cumulitative from last boot.  
# in seconds; sleep must be able to support float values
dly=3

function validate {

	error=0
	if [ -z "$1" ]
		then
			echo "Parameter 1 WARNING is missing"
			error=1
	fi
	if [ -z "$2" ]
		then
			echo "Parameter 2 CRITICAL is missing"
			error=1
	fi
	if [ $error -eq 1 ]
		then
			echo "execution: /bin/bash check_cpu.sh 40 80 (40 - warning, 80 - critical). {Third not empty parameter means limits are applied for each core, not the summary cpu utilization (not implemented yet)}"
			exit 3
	fi
}

function calculate {

#load arrays
IFS=' ' read -r -a firstarr <<< "$1"
IFS=' ' read -r -a secondarr <<< "$2"

#clear name fields in array so that calculations don't get messy
firstarr[0]=0 ;
secondarr[0]=0 ;

#clear values 
firsttotcpu=0
secondtotcpu=0

#calculate the begining interrupt counts
for f in ${firstarr[@]}; 
    do 
        let firsttotcpu+=$f; 
done
firstidle=$((${firstarr[4]}+${firstarr[5]})); 

#calculate the ending interrupt counts
for l in ${secondarr[@]}; 
    do
        let secondtotcpu+=$l; 
    done; 
secondidle=$((${secondarr[4]}+${secondarr[5]})); 

#calculate the relative change counts
insttotcpu=$(( secondtotcpu - firsttotcpu ))
instidle=$(( secondidle - firstidle ))

#calculate the utilization percentage. must be done external to bash as it's a
#floating calculation
cpu_load=$( echo | awk -v tot=$insttotcpu -v idl=$instidle ' { print ( ( ( tot - idl ) / tot ) * 100 ) } ' )

echo -n $cpu_load 


} 
export -f calculate

validate $1 $2

#main execution

oldIFS=$IFS

IFS=$'\n' cpu_start=( $( grep cpu /proc/stat ) );

#must delay to get difference
sleep $dly

IFS=$'\n' cpu_end=( $( grep cpu /proc/stat ) );

cpucount=${#cpu_start[@]}

#uncomment this for loop to enable printing the cpu name above the percentages
#for i in ${cpu_start[@]};
#    do
#        IFS=' ' read -r -a name <<< "$i"
#        echo -n ${name[0]} " "
#done
#echo ""
string=""
summary=0
for (( i=0; i<$cpucount; i++ ))
    do
		IFS=' ' read -r -a name <<< "$i"
        cpuname=${name[0]}
		
        result=`calculate "${cpu_start[$i]}" "${cpu_end[$i]}"`
		summary=`echo "$summary+$result"|bc`
		if [ $i -gt 0 ]
			then
				string=$string", "
		fi
		
		string=$string"cpu$cpuname=$result"

done

total=`echo "$summary/$i"|bc`

if [ $total -gt $2 ]
	then
		echo "CRITICAL - total cpu usage: $total% | $string"
		exit 2
fi
if [ $total -gt $1 ]
	then
		echo "WARNING - total cpu usage: $total% | $string"
		exit 1
fi

echo "OK total CPU usage: $total%|$string"
exit 0
#echo $string

IFS=$oldIFS
